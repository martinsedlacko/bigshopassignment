package com.aardwark.services;

import com.aardwark.products.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ShoppingCartTest {


    private final ShoppingCart shoppingCart = new ShoppingCart();

    @Nested
    class addItemToShoppingCart{

        @Test
        void withValidProduct(){
            Product SIMCard = new SIMCard();
            shoppingCart.addProduct(SIMCard);

            assertAll(
                    () -> assertEquals(1, shoppingCart.getProducts().size()),
                    () -> assertEquals(SIMCard.getName(), shoppingCart.getProducts().get(0).getName())
            );
        }

        @Test
        void withNull(){
            IllegalArgumentException thrownException = assertThrows(IllegalArgumentException.class,
                    () -> shoppingCart.addProduct(null));
            assertEquals("Cannot add null to shopping cart",thrownException.getMessage());
        }
    }

    @Nested
    class getTotalPrice{

        private List<Product> productList = new ArrayList<>();

        @BeforeEach
        void setUp() {
            productList.add(new PhoneCase()); //11,2
            productList.add(new PhoneInsurance()); // 96
            productList.add(new SIMCard()); // 22,4
            productList.add(new SIMCard()); // 0
            productList.add(new WiredEarphones()); // 33,6
            productList.add(new WirelessEarphones()); // 56
        }

        @Test
        void withValidProducts(){

            shoppingCart.addListOfProducts(productList);
            shoppingCart.processOrder();
            assertEquals(219.2, shoppingCart.getTotalPrice());
        }
    }
}
