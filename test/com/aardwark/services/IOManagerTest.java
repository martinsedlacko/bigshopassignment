package com.aardwark.services;

import com.aardwark.products.EProduct;
import com.aardwark.products.PhoneCase;
import com.aardwark.products.Product;
import com.aardwark.products.SIMCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class IOManagerTest {

    private final String INPUPT = "SIM card, Phone case";

    private List<String> listOfItems = new ArrayList<>();

    private List<String> listOfInvalidProductNames = new ArrayList<>();

    private List<Product> listOfProducts = new ArrayList<>();

    @BeforeEach
    void setUp(){
        listOfItems.add(EProduct.SIM_CARD.getName());
        listOfItems.add(EProduct.PHONE_CASE.getName());

        listOfProducts.add(new SIMCard());
        listOfProducts.add(new PhoneCase());

        listOfInvalidProductNames.add("case");
        listOfInvalidProductNames.add(null);
        listOfInvalidProductNames.add("insurance");
    }

    private final IOManager ioManager = new IOManager();

    @Nested
    class getListOfItemsFromInputString{

        @Test
        void withValidInput(){
            List<String> outputFromIOManager = ioManager.getListOfItemsFromInputString(INPUPT);

            assertEquals(listOfItems,outputFromIOManager);
            assertEquals(2, outputFromIOManager.size());
            assertEquals(listOfItems.get(0) , outputFromIOManager.get(0));
            assertEquals(listOfItems.get(1), outputFromIOManager.get(1));
        }

        @Test
        void withEmptySpaceInput() {
            IllegalArgumentException thrownException = assertThrows(IllegalArgumentException.class,
                    () -> ioManager.getListOfItemsFromInputString("       "));
            assertEquals("Input  string cannot be empty spaces", thrownException.getMessage());
        }

        @Test
        void withNullInput(){
            IllegalArgumentException thrownException = assertThrows(IllegalArgumentException.class,
                    () -> ioManager.getListOfItemsFromInputString(null));
            assertEquals("Input string cannot be null", thrownException.getMessage());
        }
    }

    @Nested
    class getListOfProductsFromListOfString{

        @Test
        void withValidListOfString(){
            List<Product> listOfProductsFromIOManager = ioManager.getListOfProductsFromInputList(listOfItems);

            assertAll(
                    () -> assertEquals(listOfProducts.get(0).getName(), listOfProductsFromIOManager.get(0).getName()),
                    () -> assertEquals(listOfProducts.get(1).getName(), listOfProductsFromIOManager.get(1).getName()),
                    () -> assertEquals(listOfProducts.size(), listOfProductsFromIOManager.size())
            );
        }

        @Test
        void withNullList(){
            IllegalArgumentException thrownException = assertThrows(IllegalArgumentException.class,
                    () -> ioManager.getListOfProductsFromInputList(null));
            assertEquals("List cannot be null", thrownException.getMessage());
        }

        @Test
        void withInvalidItemsInList(){
            List<Product> listOfProductsFromIOManager = ioManager.getListOfProductsFromInputList(listOfInvalidProductNames);

            assertTrue(listOfProductsFromIOManager.isEmpty());
        }
    }

}
