package com.aardwark.services;

import com.aardwark.products.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DiscountManagerTest {

    private List<Product> productList = new ArrayList<>();

    private DiscountManager discountManager = new DiscountManager();


    @BeforeEach
    void setUp() {
        productList.add(new PhoneCase());
        productList.add(new SIMCard());
        productList.add(new WiredEarphones());
        productList.add(new PhoneInsurance());
    }

    @Test
    void shouldSetDiscountForInsurance(){

        discountManager.setDiscountForAllProducts(productList);

        assertAll(
                () -> assertEquals(0.0,productList.get(0).getDiscount()),
                () -> assertEquals(0.0,productList.get(1).getDiscount()),
                () -> assertEquals(0.0,productList.get(2).getDiscount()),
                () -> assertEquals(24.0,productList.get(3).getDiscount())
        );
    }

    @Test
    void shouldSetDiscountForSIMCard(){
        productList.add(new SIMCard());

        discountManager.setDiscountForAllProducts(productList);

        assertAll(
                () -> assertEquals(0.0,productList.get(0).getDiscount()),
                () -> assertEquals(22.4,productList.get(1).getDiscount()),
                () -> assertEquals(0.0,productList.get(2).getDiscount()),
                () -> assertEquals(24.0,productList.get(3).getDiscount()),
                () -> assertEquals(0.0,productList.get(4).getDiscount())
        );
    }

}
