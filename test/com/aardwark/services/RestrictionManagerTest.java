package com.aardwark.services;

import com.aardwark.products.PhoneInsurance;
import com.aardwark.products.Product;
import com.aardwark.products.SIMCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RestrictionManagerTest {

    private List<Product> productList ;
    @BeforeEach
    void setUp() {
       productList = new ArrayList<>();
    }

    @Nested
    class SIMCardRestrictions{
        @Test
        void testWithMoreSimCardThanAllowed(){
            for (int i = 0; i< 11; i++)
            {
                productList.add(new SIMCard());
            }

            IllegalArgumentException thrownException = assertThrows(IllegalArgumentException.class, () -> RestrictionManager.checkRestrictions(productList));
            assertEquals("You cannot buy more than 10 SIM Card", thrownException.getMessage());
        }

        @Test
        void testWithAllowedSimCardCount(){
            for (int i = 0; i< 10; i++)
            {
                productList.add(new SIMCard());
            }

            assertDoesNotThrow(() -> RestrictionManager.checkRestrictions(productList));
        }
    }

    @Nested
    class PhoneInsuranceRestriction{
        @Test
        void testWithMorePhoneInsurancesThanAllowed(){
            productList.add(new PhoneInsurance());
            productList.add(new PhoneInsurance());

            IllegalArgumentException thrownException = assertThrows(IllegalArgumentException.class, () -> RestrictionManager.checkRestrictions(productList));
            assertEquals("You cannot buy more than 1 Phone insurance", thrownException.getMessage());
        }

        @Test
        void testWithAllowedSimCardCount(){
            productList.add(new PhoneInsurance());
            assertDoesNotThrow(() -> RestrictionManager.checkRestrictions(productList));
        }

    }
}
