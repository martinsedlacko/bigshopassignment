package com.aardwark.services;

import com.aardwark.products.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TaxManagerTest {

    private List<Product> productList = new ArrayList<>();

    private TaxManager taxManager = new TaxManager();


    @BeforeEach
    void setUp() {
        productList.add(new PhoneCase());
        productList.add(new SIMCard());
        productList.add(new WiredEarphones());
        productList.add(new PhoneInsurance());
    }

    @Test
    void shouldSetTaxForAllProducts(){

        taxManager.setTaxForAllProducts(productList);

        assertAll(
                () -> assertEquals(1.2,productList.get(0).getTax()),
                () -> assertEquals(2.4,productList.get(1).getTax()),
                () -> assertEquals(3.6,productList.get(2).getTax()),
                () -> assertEquals(0.0,productList.get(3).getTax())
        );
    }

}
