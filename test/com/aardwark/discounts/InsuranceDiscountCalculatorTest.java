package com.aardwark.discounts;

import com.aardwark.products.PhoneInsurance;
import com.aardwark.products.Product;
import com.aardwark.products.WiredEarphones;
import com.aardwark.products.WirelessEarphones;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InsuranceDiscountCalculatorTest {

    private InsuranceDiscountCalculator insuranceDiscountCalculator = new InsuranceDiscountCalculator();

    @Test
    void insuranceShouldBeDiscountedConditionCompliantWithWirelessEarphones() {
        List<Product> productList = new ArrayList<>();
        productList.add(new PhoneInsurance());
        productList.add(new WirelessEarphones());

        assertEquals(24.0, insuranceDiscountCalculator.calculate(productList));
    }

    @Test
    void insuranceShouldBeDiscountedConditionCompliantWithWiredEarphones() {
        List<Product> productList = new ArrayList<>();
        productList.add(new PhoneInsurance());
        productList.add(new WiredEarphones());

        assertEquals(24.0, insuranceDiscountCalculator.calculate(productList));
    }

    @Test
    void insuranceShouldNotBeDiscounted() {
        List<Product> productList = new ArrayList<>();
        productList.add(new PhoneInsurance());

        assertEquals(0.0, insuranceDiscountCalculator.calculate(productList));
    }
}
