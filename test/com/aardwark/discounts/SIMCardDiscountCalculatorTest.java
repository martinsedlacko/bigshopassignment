package com.aardwark.discounts;

import com.aardwark.products.PhoneInsurance;
import com.aardwark.products.Product;
import com.aardwark.products.SIMCard;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SIMCardDiscountCalculatorTest {

    private SIMCardDiscountCalculator simCardDiscountCalculator = new SIMCardDiscountCalculator();

    @Test
    void simCardShouldBeDiscounted(){
        List<Product> productList = new ArrayList<>();
        productList.add(new SIMCard());
        productList.add(new SIMCard());
        productList.add(new SIMCard());
        productList.add(new SIMCard());
        productList.add(new SIMCard());
        productList.add(new SIMCard());


        assertEquals(22.4, simCardDiscountCalculator.calculate(productList));
    }

    @Test
    void simCardShouldNotBeDiscounted(){
        List<Product> productList = new ArrayList<>();
        productList.add(new SIMCard());

        assertEquals(0.0, simCardDiscountCalculator.calculate(productList));
    }
}
