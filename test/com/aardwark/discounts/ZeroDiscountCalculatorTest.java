package com.aardwark.discounts;

import com.aardwark.products.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ZeroDiscountCalculatorTest {

    private ZeroDiscountCalculator zeroDiscountCalculator = new ZeroDiscountCalculator();

    private List<Product> productList = new ArrayList<>();

    @BeforeEach
    void setUp() {
        productList.add(new PhoneCase());
        productList.add(new SIMCard());
        productList.add(new WiredEarphones());
        productList.add(new PhoneInsurance());
    }

    @Test
    void shouldSetZeroDiscount(){
        zeroDiscountCalculator.calculate(productList);

        assertEquals(0.0, zeroDiscountCalculator.calculate(productList));
    }
}
