package com.aardwark.taxes;

import com.aardwark.products.PhoneCase;
import com.aardwark.products.Product;
import com.aardwark.products.SIMCard;
import com.aardwark.products.WiredEarphones;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BasicTaxCalculatorTest {


    private final BasicTaxCalculator basicTaxCalculator = new BasicTaxCalculator();

    private List<Product> productList = new ArrayList<>();

    @BeforeEach
    void setUp() {
        productList.add(new PhoneCase());
        productList.add(new SIMCard());
        productList.add(new WiredEarphones());
    }

    @Test
    void calculateTaxesWithValidProducts(){
        assertAll(
                () -> assertEquals(2.4,basicTaxCalculator.calculate(productList.get(1))),
                () -> assertEquals(1.2, basicTaxCalculator.calculate(productList.get(0))),
                () -> assertEquals(3.6, basicTaxCalculator.calculate(productList.get(2)))
        );
    }
}
