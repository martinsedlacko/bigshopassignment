package com.aardwark.taxes;

import com.aardwark.products.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InsuranceTaxCalculatorTest {

    private final InsuranceTaxCalculator insuranceCalculator = new InsuranceTaxCalculator();

    private List<Product> productList = new ArrayList<>();

    @BeforeEach
    void setUp() {
        productList.add(new PhoneInsurance());
    }

    @Test
    void calculateTaxesWithValidProducts(){
        assertEquals(0.0, insuranceCalculator.calculate(productList.get(0)));
    }
}
