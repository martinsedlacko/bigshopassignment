package com.aardwark.taxes;

import com.aardwark.products.PhoneInsurance;
import com.aardwark.products.SIMCard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class TaxCalculatorFactoryTest {

    @Test
    void shouldReturnBasicTaxCalculatorTest(){

        assertTrue(TaxCalculatorFactory.getInstance(new SIMCard()) instanceof BasicTaxCalculator);
    }

    @Test
    void shouldReturnInsuranceTaxCalculatorTest(){

        assertTrue(TaxCalculatorFactory.getInstance(new PhoneInsurance()) instanceof InsuranceTaxCalculator);
    }
}
