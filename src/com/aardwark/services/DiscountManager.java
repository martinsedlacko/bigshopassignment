package com.aardwark.services;

import com.aardwark.discounts.DiscountCalculatorFactory;
import com.aardwark.discounts.IDiscountCalculator;
import com.aardwark.products.Product;

import java.util.List;

class DiscountManager {

    void setDiscountForAllProducts(List<Product> productList)
    {
        for(Product product : productList)
        {
            IDiscountCalculator discountCalculator = DiscountCalculatorFactory.getInstance(product);

            double discount = discountCalculator.calculate(productList);
            if(discount > 0) {
                product.setDiscounted(true);
            }
            product.setDiscount(discount);
        }
    }
}
