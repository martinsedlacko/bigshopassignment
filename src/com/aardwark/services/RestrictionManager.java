package com.aardwark.services;

import com.aardwark.discounts.SIMCardDiscountCalculator;
import com.aardwark.products.EProduct;
import com.aardwark.products.Product;

import java.util.List;

public class RestrictionManager {
    private static final int MAX_SIMCARD_COUNT = 10;
    private static final int MAX_PHONE_INSURANCE_COUNT = 1;

    private RestrictionManager() {
    }

    public static void checkRestrictions(List<Product> productList){

        if(SIMCardDiscountCalculator.getSimCardCount(productList) > MAX_SIMCARD_COUNT)
            throw new IllegalArgumentException("You cannot buy more than 10 SIM Card");
        if(getPhoneInsuranceCount(productList) > MAX_PHONE_INSURANCE_COUNT)
            throw new IllegalArgumentException("You cannot buy more than 1 Phone insurance");

    }

    private static int getPhoneInsuranceCount(List<Product> productList) {
        return (int) productList.stream()
                .filter(product -> product.getName().equals(EProduct.PHONE_INSURANCE.getName()))
                .count();
    }
}
