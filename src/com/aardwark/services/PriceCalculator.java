package com.aardwark.services;

import com.aardwark.products.Product;

import java.util.List;

class PriceCalculator {

    private double totalPrice = 0.0;

    double calculateTotalPrice(List<Product> products){

        for(Product product : products)
        {
            totalPrice += product.getBruttoPrice() - product.getDiscount();
        }

        return totalPrice;
    }
}
