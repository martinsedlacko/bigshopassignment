package com.aardwark.services;

import com.aardwark.products.Product;


public class InvoiceManager {

    private static final String TAB = " CHF\t|";

    private StringBuilder invoice = new StringBuilder();

    private final ShoppingCart shoppingCart ;

    public InvoiceManager(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
        this.invoice.append("Product name \t| Netto \t| Tax \t| Discount \t| Brutto \n");
    }

    public void printInvoice(){
        this.generateInvoice();
        System.out.println(invoice.toString());
    }

    private void generateInvoice() {

        for(Product product : shoppingCart.getProducts()){
        this.invoice.append(product.getName())
                .append("\t|")
                .append(product.getNettoPrice())
                .append(TAB)
                .append(product.getTax())
                .append(TAB)
                .append(product.getDiscount())
                .append(TAB)
                .append(product.getBruttoPriceWithDiscount())
                .append(" CHF\n");
        }
        this.addTotalPrice(shoppingCart.getTotalPrice());
    }

    private void addTotalPrice(double totalPrice){
        this.invoice.append("Total price |").append(totalPrice).append(" CHF");
    }
}
