package com.aardwark.services;

import com.aardwark.products.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ShoppingCart {

    private List<Product> products = new ArrayList<>();

    private PriceCalculator priceCalculator = new PriceCalculator();

    private TaxManager taxManager = new TaxManager();

    private DiscountManager discountManager = new DiscountManager();

    private double totalPrice;

    public double getTotalPrice() {
        return this.totalPrice;
    }

    public void processOrder(){
        taxManager.setTaxForAllProducts(products);
        discountManager.setDiscountForAllProducts(products);

        totalPrice = priceCalculator.calculateTotalPrice(products);
    }

    public List<Product> getProducts() {
        return products;
    }

    ShoppingCart addProduct(Product product){
        if(Objects.isNull(product)) throw new IllegalArgumentException("Cannot add null to shopping cart");

        products.add(product);
        return this;
    }

    public void addListOfProducts(List<Product> productList) {
        products.addAll(productList);
    }

}
