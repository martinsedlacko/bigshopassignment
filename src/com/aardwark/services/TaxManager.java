package com.aardwark.services;

import com.aardwark.products.Product;
import com.aardwark.taxes.TaxCalculator;
import com.aardwark.taxes.TaxCalculatorFactory;

import java.util.List;

class TaxManager {


    void setTaxForAllProducts(List<Product> products) {
        for(Product product : products)
        {
            TaxCalculator taxCalculator = TaxCalculatorFactory.getInstance(product);

            double tax = taxCalculator.calculate(product);
            product.setTax(tax);
        }
    }
}
