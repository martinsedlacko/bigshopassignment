package com.aardwark.services;

import com.aardwark.products.Product;
import com.aardwark.products.ProductFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class IOManager {

    public List<Product> getListOfProducts() throws IOException {
        String inputString = this.getInputFromConsole();
        List<String> listOfInputProducts = this.getListOfItemsFromInputString(inputString);
        return this.getListOfProductsFromInputList(listOfInputProducts);
    }

    private String getInputFromConsole() throws IOException {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            return bufferedReader.readLine();
        } catch (IOException e) {
            throw new IOException("Input should be in format \"SIM card\", \"phone case\"");
        }
    }

    List<String> getListOfItemsFromInputString(String input) {
        if (input == null) throw new IllegalArgumentException("Input string cannot be null");
        if (input.trim().equals("")) throw new IllegalArgumentException("Input  string cannot be empty spaces");

        return  Arrays.stream(input.split(","))
                .map(String::trim)
                .collect(Collectors.toList());
    }

    List<Product> getListOfProductsFromInputList(List<String> inputList) {
        if(inputList == null) throw new IllegalArgumentException("List cannot be null");

        return inputList
                .stream()
                .filter(Objects::nonNull)
                .map(ProductFactory::getProduct)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }
}
