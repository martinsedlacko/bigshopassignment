package com.aardwark.discounts;

import com.aardwark.products.EProduct;
import com.aardwark.products.PhoneInsurance;
import com.aardwark.products.Product;

import java.util.List;

import static java.lang.Math.round;

public class InsuranceDiscountCalculator implements IDiscountCalculator{
    private static final double INSURANCE_TAX = 20;


    @Override
    public double calculate(List<Product> productList) {
        return productList
                .stream().noneMatch(product ->
                        ( product.getName().equals(EProduct.WIRELESS_EARPHONES.getName()) || product.getName().equals(EProduct.WIRED_EARPHONES.getName())))
                ? 0.0
                : (round(new PhoneInsurance().getNettoPrice() * (INSURANCE_TAX / 100.0) * 10.0) / 10.0);
    }

}