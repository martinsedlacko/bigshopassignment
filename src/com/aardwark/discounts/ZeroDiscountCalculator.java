package com.aardwark.discounts;

import com.aardwark.products.Product;

import java.util.List;

public class ZeroDiscountCalculator implements IDiscountCalculator {

    @Override
    public double calculate(List<Product> productList) {
        return 0;
    }
}
