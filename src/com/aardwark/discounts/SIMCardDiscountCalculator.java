package com.aardwark.discounts;

import com.aardwark.products.Product;
import com.aardwark.products.SIMCard;
import com.aardwark.taxes.TaxCalculator;
import com.aardwark.taxes.TaxCalculatorFactory;

import java.util.List;

public class SIMCardDiscountCalculator implements IDiscountCalculator {

    @Override
    public double calculate(List<Product> productList) {
        int simCardCount = getSimCardCount(productList);
        int simCardToDiscountCount = (int) Math.floor(simCardCount / 2.0);
        int alreadyDiscountedSimCardsCount = this.getDiscountedSimCardCount(productList);

        if(alreadyDiscountedSimCardsCount < simCardToDiscountCount)
        {
           return getDiscountPerSimCard();
        }
        return 0.0;
    }


    private double getDiscountPerSimCard(){
        TaxCalculator taxCalculator = TaxCalculatorFactory.getInstance(new SIMCard());
        return new SIMCard().getNettoPrice() + taxCalculator.calculate(new SIMCard());
    }

    public static int getSimCardCount(List<Product> productList) {
        return (int) productList.stream().filter(product -> product.getName().equals("SIM card")).count();
    }

    private int getDiscountedSimCardCount(List<Product> productList)
    {
        return (int) productList.stream().filter(product -> product instanceof SIMCard)
                .filter(Product::isDiscounted)
                .count();
    }
}
