package com.aardwark.discounts;

import com.aardwark.products.Product;

import java.util.List;

public interface IDiscountCalculator {
    double calculate(List<Product> productList);
}
