package com.aardwark.discounts;

import com.aardwark.products.PhoneInsurance;
import com.aardwark.products.Product;
import com.aardwark.products.SIMCard;

public class DiscountCalculatorFactory {
    private DiscountCalculatorFactory() {
    }

    public static IDiscountCalculator getInstance(Product product)
    {
        if(product instanceof PhoneInsurance)
            return new InsuranceDiscountCalculator();
        if(product instanceof SIMCard)
            return new SIMCardDiscountCalculator();
        return new ZeroDiscountCalculator();
    }
}
