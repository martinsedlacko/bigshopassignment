package com.aardwark.taxes;

import com.aardwark.products.Product;

import static java.lang.Math.round;

public class BasicTaxCalculator implements TaxCalculator {
    private final static double TAX = 12.0;

    @Override
    public double calculate(Product product) {
        double tax = round(product.getNettoPrice() * (TAX / 100) * 10);
        return tax / 10;
    }
}
