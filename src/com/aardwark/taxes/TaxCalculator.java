package com.aardwark.taxes;

import com.aardwark.products.Product;

public interface TaxCalculator {
    double calculate(Product product);
}
