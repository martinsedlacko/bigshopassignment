package com.aardwark.taxes;

import com.aardwark.products.PhoneInsurance;
import com.aardwark.products.Product;

public class TaxCalculatorFactory {

    private TaxCalculatorFactory() {
    }

    public static TaxCalculator getInstance(Product product){
        if (product instanceof PhoneInsurance)
            return new InsuranceTaxCalculator();
        else
            return new BasicTaxCalculator();
    }
}
