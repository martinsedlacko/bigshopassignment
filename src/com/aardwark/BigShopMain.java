package com.aardwark;

import com.aardwark.products.Product;
import com.aardwark.services.IOManager;
import com.aardwark.services.InvoiceManager;
import com.aardwark.services.RestrictionManager;
import com.aardwark.services.ShoppingCart;

import java.io.IOException;
import java.util.List;

public class BigShopMain {

    public static void main(String[] args) throws IOException {
        IOManager ioManager = new IOManager();

        System.out.println("Welcome in Big Shop. You can buy SIM Card, Phone case, Phone insurance, Wired Earphones and Wireless earphones here.");

        List<Product> productList =  ioManager.getListOfProducts();

        RestrictionManager.checkRestrictions(productList);

        ShoppingCart shoppingCart = new ShoppingCart();

        shoppingCart.addListOfProducts(productList);

        shoppingCart.processOrder();

        InvoiceManager invoiceManager = new InvoiceManager(shoppingCart);

        invoiceManager.printInvoice();
    }
}
