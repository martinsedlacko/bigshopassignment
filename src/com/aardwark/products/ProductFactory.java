package com.aardwark.products;


import java.util.Optional;

public class ProductFactory {

    private ProductFactory(){}

    public static Optional<Product> getProduct(String productName)
    {
        switch (productName.toUpperCase()){
            case "PHONE CASE" : return Optional.of(new PhoneCase());
            case "PHONE INSURANCE" : return Optional.of( new PhoneInsurance());
            case "SIM CARD" : return Optional.of(new SIMCard());
            case "WIRED EARPHONES" : return Optional.of(new WiredEarphones());
            case "WIRELESS EARPHONES" : return Optional.of(new WirelessEarphones());
            default:
                return Optional.empty();
        }
    }
}
