package com.aardwark.products;

public class PhoneInsurance implements Product {

    private final String name = EProduct.PHONE_INSURANCE.getName();

    private final static double nettoPrice = 120.0;

    private double tax = 0.0;

    private double discount = 0.0;

    private boolean isDiscounted = false;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getNettoPrice() {
        return nettoPrice;
    }

    @Override
    public double getTax() {
        return tax;
    }

    @Override
    public void setTax(double tax) {
        this.tax = tax;
    }

    @Override
    public double getBruttoPrice() {
        return nettoPrice + tax;
    }

    @Override
    public double getBruttoPriceWithDiscount() {
        return getBruttoPrice() - discount;
    }

    @Override
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public double getDiscount() {
        return this.discount;
    }

    @Override
    public void setDiscounted(boolean discounted) {
        this.isDiscounted = discounted;
    }

    @Override
    public boolean isDiscounted() {
        return this.isDiscounted;
    }
}
