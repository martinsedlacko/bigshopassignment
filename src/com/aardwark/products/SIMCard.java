package com.aardwark.products;

public class SIMCard implements Product {

    private final String name = EProduct.SIM_CARD.getName();

    private static final double nettoPrice = 20.0;

    private  double tax = 0.0;

    private double discount = 0.0;

    private boolean isDiscounted = false;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getNettoPrice() {
        return nettoPrice;
    }

    @Override
    public double getTax() {
        return tax;
    }

    @Override
    public void setTax(double tax) {
        this.tax = tax;
    }

    @Override
    public double getBruttoPrice() {
        return nettoPrice + tax;
    }

    @Override
    public double getBruttoPriceWithDiscount() {
        return getBruttoPrice() - discount;
    }

    @Override
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public double getDiscount() {
        return this.discount;
    }

    @Override
    public boolean isDiscounted() {
        return isDiscounted;
    }

    @Override
    public void setDiscounted(boolean discounted) {
        isDiscounted = discounted;
    }
}
