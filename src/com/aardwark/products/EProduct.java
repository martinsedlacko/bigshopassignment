package com.aardwark.products;

public enum EProduct {


    PHONE_CASE ("Phone case"),
    PHONE_INSURANCE ("Phone insurance"),
    SIM_CARD ("SIM card"),
    WIRED_EARPHONES ("Wired earphones"),
    WIRELESS_EARPHONES ("Wireless earphones");


    private final String name;

    EProduct(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
    public boolean equalsName(String otherName){
        return name.equals(otherName);
    }
}
