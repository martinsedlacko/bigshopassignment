package com.aardwark.products;

public interface Product {

    String getName();
    double getNettoPrice();
    void setTax(double tax);
    double getTax();
    void setDiscount(double discount);
    double getDiscount();
    double getBruttoPrice();
    double getBruttoPriceWithDiscount();
    void setDiscounted(boolean discounted);
    boolean isDiscounted();
}
